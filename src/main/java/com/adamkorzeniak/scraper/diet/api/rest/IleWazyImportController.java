package com.adamkorzeniak.scraper.diet.api.rest;

import com.adamkorzeniak.scraper.common.infrastructure.DocumentProvider;
import com.adamkorzeniak.scraper.diet.api.rest.model.IngredientResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/api/diet/ingredients")
class IleWazyImportController {

    private final DocumentProvider documentProvider;

    @GetMapping("ilewazy")
    public ResponseEntity<IngredientResponse> getIngredients(
            @RequestParam String name, @RequestParam String url) {
        Document document = documentProvider.retrieveDocument(url);
        IngredientResponse response = extractIngredientElement(name, document);
        return ResponseEntity.ok(response);
    }

    private IngredientResponse extractIngredientElement(String name, Document doc) {
        Element ingredients = doc.getElementById("ilewazy-ingedients");
        Objects.requireNonNull(ingredients);
        Elements nutritionInfos = ingredients.getElementsByClass("nutrition-label");
        Map<String, Double> nutritions = nutritionInfos.stream()
                .map(this::buildNutritionInfo)
                .filter(Objects::nonNull)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        return extractIngredients(name, nutritions);
    }

    private IngredientResponse extractIngredients(String name, Map<String, Double> nutritions) {
        IngredientResponse dto = new IngredientResponse();
        dto.setName(name);
        dto.setCalories(nutritions.get("energia"));
        dto.setProteins(nutritions.get("białko"));
        dto.setCarbs(nutritions.get("węglowodany"));
        dto.setFats(nutritions.get("tłuszcz"));
        dto.setRoughage(nutritions.get("błonnik"));
        dto.setSalt(nutritions.get("sól"));
        return dto;
    }

    private Map.Entry<String, Double> buildNutritionInfo(Element element) {
        String key = element.text().toLowerCase().trim();
        String elementContent = element.nextElementSibling().text().trim();
        Pattern p = Pattern.compile("\\d[\\d,.]*");
        Matcher matcher = p.matcher(elementContent);
        if (!matcher.find()) {
            return null;
        }
        String value = matcher.group().replace(",", ".");
        return Map.entry(key, Double.valueOf(value));
    }

}
