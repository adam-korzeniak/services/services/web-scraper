package com.adamkorzeniak.scraper.common.infrastructure;

import com.adamkorzeniak.scraper.common.infrastructure.exception.DocumentRetrievingException;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Slf4j
@Service
public class DocumentProvider {

    public Document retrieveDocument(String pageUrl) {
        try {
            return Jsoup.connect(pageUrl).get();
        } catch (IOException e) {
            throw new DocumentRetrievingException();
        }
    }

}
