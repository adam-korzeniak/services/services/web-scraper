package com.adamkorzeniak.scraper.movie.api.rest;

import lombok.Data;

import java.util.List;

@Data
public class MovieDTO {

    private String title;
    private String titleOriginal;
    private Integer year;
    private Integer duration;
    private List<GenreDTO> genres;
}
