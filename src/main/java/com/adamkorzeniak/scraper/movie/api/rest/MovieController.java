package com.adamkorzeniak.scraper.movie.api.rest;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/api/movies")
class MovieController {

    private final FilmwebService filmwebService;

    @GetMapping()
    public ResponseEntity<MovieDTO> getMovies(@RequestParam String url) {
        MovieDTO movie = filmwebService.getMovieDetails(url);
        return ResponseEntity.ok(movie);
    }
    @PostMapping
    public ResponseEntity<List<MovieDTO>> getMovies(@RequestBody List<String> urls) {
        List<MovieDTO> response = urls.stream()
                .map(filmwebService::getMovieDetails)
                .collect(Collectors.toList());
        return ResponseEntity.ok(response);
    }
    @PostMapping("new")
    public ResponseEntity<List<String>> getMoviesNew(@RequestBody String names) {
        List<String> response = names.lines()
                .map(filmwebService::getMovieUrl)
                .collect(Collectors.toList());
        return ResponseEntity.ok(response);
    }
}
