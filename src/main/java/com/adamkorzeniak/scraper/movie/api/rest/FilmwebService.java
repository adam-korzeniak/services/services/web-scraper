package com.adamkorzeniak.scraper.movie.api.rest;

import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class FilmwebService {

    public MovieDTO getMovieDetails(String movieUrl) {
        Document doc = retrieveDocument(movieUrl);
        if (doc == null) {
            return null;
        }

        MovieDTO movie = new MovieDTO();
        movie.setTitle(retrieveTitle(doc));

        movie.setTitleOriginal(retrieveOriginalTitle(doc));
        movie.setYear(retrieveYear(doc));
        movie.setDuration(retrieveDuration(doc));
        movie.setGenres(retrieveGenres(doc));

        return movie;
    }

    private Document retrieveDocument(String movieUrl) {
        Document doc = null;
        try {
            doc = Jsoup.connect(movieUrl).get();
        } catch (IOException e) {
            log.error("context", e);
        }
        return doc;
    }

    private String retrieveTitle(Document doc) {
        Element titleElement = doc.select(".filmCoverSection__title span").first();
        if (titleElement == null) {
            return null;
        }
        return titleElement.text();
    }

    private String retrieveOriginalTitle(Document doc) {
        Element originalTitleElement = doc.select(".filmCoverSection__orginalTitle").first();
        if (originalTitleElement == null) {
            return retrieveTitle(doc);
        }
        return originalTitleElement.text().trim();
    }

    private Integer retrieveYear(Document doc) {
        Element yearElement = doc.select(".filmCoverSection__year").first();
        if (yearElement == null) {
            return null;
        }
        String year = yearElement.text();
        return Integer.parseInt(year);
    }

    private Integer retrieveDuration(Document doc) {
        Element stringElement = doc.select(".filmCoverSection__filmTime").first();
        if (stringElement == null) {
            return null;
        }
        String duration = stringElement.attr("data-duration");
        return Integer.parseInt(duration);
    }

    private List<GenreDTO> retrieveGenres(Document doc) {
        Elements genreElements = doc.select(".filmInfo__info[itemprop=genre] span a");
        if (genreElements.isEmpty()) {
            return new ArrayList<>();
        }
        return genreElements.stream()
                .map(element -> {
                    GenreDTO dto = new GenreDTO();
                    dto.setName(element.text());
                    return dto;
                })
                .collect(Collectors.toList());
    }

    public String getMovieUrl(String name) {
        String movieUrl = "https://www.filmweb.pl/search?q=" + name.trim().replaceAll("\s", "+");
        Document doc = retrieveDocument(movieUrl);
        Element element = doc.select(".filmPreview__link").first();
        if (element == null) {
            return null;
        }
        String link = element.attr("href");
        return "https://www.filmweb.pl" + link;
    }
}
