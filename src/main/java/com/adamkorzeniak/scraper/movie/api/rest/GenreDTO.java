package com.adamkorzeniak.scraper.movie.api.rest;

import lombok.Data;

@Data
public class GenreDTO {
    private String name;
}
