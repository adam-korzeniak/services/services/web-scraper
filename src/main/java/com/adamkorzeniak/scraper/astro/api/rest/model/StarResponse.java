package com.adamkorzeniak.scraper.astro.api.rest.model;

import lombok.Data;
import lombok.Generated;

@Data
@Generated
public class StarResponse {

    private String name;
    private String type;
    private String constellation;

    private Double magnitude;
    private Double sizeBigger;
    private Double sizeSmaller;

    private RightAscension rightAscension;
    private Declination declination;

    @Data
    public static class RightAscension {
        private Integer hours;
        private Integer minutes;
        private Double seconds;
    }

    @Data
    public static class Declination {
        private boolean isPositive;
        private Integer degrees;
        private Integer minutes;
        private Double seconds;
    }
}
