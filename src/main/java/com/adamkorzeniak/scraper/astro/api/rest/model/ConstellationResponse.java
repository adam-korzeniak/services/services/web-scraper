package com.adamkorzeniak.scraper.astro.api.rest.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Generated;

import java.util.Set;

@Data
@Generated
@AllArgsConstructor
public class ConstellationResponse {
    private String name;
    private Set<StarResponse> star;
    private Set<DeepSkyResponse> deepSky;
}
