package com.adamkorzeniak.scraper.astro.api.rest;

import com.adamkorzeniak.scraper.astro.api.rest.model.ConstellationResponse;
import com.adamkorzeniak.scraper.astro.api.rest.model.DeepSkyResponse;
import com.adamkorzeniak.scraper.astro.api.rest.model.StarResponse;
import com.adamkorzeniak.scraper.common.infrastructure.DocumentProvider;
import lombok.RequiredArgsConstructor;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/api/astro")
class AstroSimbadImportController {
    private static final String HOST_URL = "https://theskylive.com";
    private static final String BASE_URL = HOST_URL + "/sky/";
    private static final String CONSTELLATIONS_URL = BASE_URL + "constellations/";
    private static final String DEEP_SKY_URL = BASE_URL + "deepsky/";

    private final DocumentProvider documentProvider;

    @GetMapping("constellations")
    public ResponseEntity<Set<ConstellationResponse>> getAllConstellations() {
        Document document = documentProvider.retrieveDocument(CONSTELLATIONS_URL);
        Set<String> constellationLinks = document.select(".object_container table a").stream()
                .map(el -> HOST_URL + el.attr("href"))
                .collect(Collectors.toSet());
        Set<ConstellationResponse> objectLinks = constellationLinks.stream()
                .limit(10)
                .map(this::buildConstellations)
                .collect(Collectors.toSet());

        return ResponseEntity.ok(objectLinks);
    }

    private ConstellationResponse buildConstellations(String constellationUrl) {
        Document document = documentProvider.retrieveDocument(constellationUrl);

        Elements elements = document.getElementsByClass("objectData").prev();
        Set<StarResponse> starResponse = buildStarResponses(HOST_URL + elements.get(0).getElementsByTag("a").first().attr("href"));
        Set<DeepSkyResponse> deepSkyResponse = buildDeepSkyResponses(HOST_URL + elements.get(1).getElementsByTag("a").first().attr("href"));
        return new ConstellationResponse(constellationUrl, starResponse, deepSkyResponse);
    }

    private Set<StarResponse> buildStarResponses(String url) {
        //TODO: Provide functionality
        return Collections.emptySet();
    }

    private Set<DeepSkyResponse> buildDeepSkyResponses(String url) {
        Document document = documentProvider.retrieveDocument(url);
        return document.getElementsByClass("data").stream()
                .limit(10)
                .map(el -> HOST_URL + el.getElementsByTag("a").first().attr("href"))
                .map(this::buildDeepSkyResponse)
                .collect(Collectors.toSet());
    }

    private DeepSkyResponse buildDeepSkyResponse(String url) {
        Document document = documentProvider.retrieveDocument(url);
        Element mainContent = getMainContent(document);
        return buildResponse(mainContent);
    }

    @GetMapping("deepsky")
    public ResponseEntity<DeepSkyResponse> getDeepSkyObjects(String name) {
        Document document = documentProvider.retrieveDocument(DEEP_SKY_URL + name);
        Element mainContent = getMainContent(document);
        DeepSkyResponse response = buildResponse(mainContent);
        return ResponseEntity.ok(response);
    }

    private DeepSkyResponse buildResponse(Element mainContent) {
        String name = getElementText(mainContent, "Object Name");
        String type = getElementText(mainContent, "Object Type");
        String constellation = getElementText(mainContent, "Constellation");
        String magnitude = getElementText(mainContent, "Magnitude V");
        String biggerSize = getElementText(mainContent, "Major Angular Size");
        String smallerSize = getElementText(mainContent, "Minor Angular Size");
        String rightAscension = getElementText(mainContent, "Right Ascension J2000");
        String declination = getElementText(mainContent, "Declination J2000");

        DeepSkyResponse response = new DeepSkyResponse();
        response.setName(name);
        response.setType(type);
        response.setConstellation(constellation);
        if (!magnitude.isBlank()) {
            response.setMagnitude(Double.parseDouble(magnitude));
        }
        response.setSizeBigger(Double.parseDouble(biggerSize.replace("arcmin", "").trim()));
        response.setSizeSmaller(Double.parseDouble(smallerSize.replace("arcmin", "").trim()));
        response.setRightAscension(buildRightAscension(rightAscension));
        response.setDeclination(buildDeclination(declination));
        return response;
    }

    private DeepSkyResponse.Declination buildDeclination(String declination) {
        Pattern p = Pattern.compile("([+-])(\\d+)° (\\d+)’ (\\d+)”");
        Matcher m = p.matcher(declination);
        DeepSkyResponse.Declination result = new DeepSkyResponse.Declination();
        if (m.find()) {
            result.setPositive("+".equals(m.group(1)));
            result.setDegrees(Integer.parseInt(m.group(2)));
            result.setMinutes(Integer.parseInt(m.group(3)));
            result.setSeconds(Double.parseDouble(m.group(4)));
        }
        return result;
    }

    private DeepSkyResponse.RightAscension buildRightAscension(String rightAscension) {
        Pattern p = Pattern.compile("(\\d+)h (\\d+)m (\\d+)s");
        Matcher m = p.matcher(rightAscension);
        DeepSkyResponse.RightAscension result = new DeepSkyResponse.RightAscension();
        if (m.find()) {
            result.setHours(Integer.parseInt(m.group(1)));
            result.setMinutes(Integer.parseInt(m.group(2)));
            result.setSeconds(Double.parseDouble(m.group(3)));
        }
        return result;
    }

    private Element getMainContent(Document document) {
        Elements elements = document.getElementsByClass("main_content");
        if (elements.size() != 1) {
            throw new RuntimeException();
        }
//        Elements xxx = elements.first().getElementsByClass("keyinfobox");
//        String label = "";
//        Elements select = xxx.select(String.format(".keyinfobox label:contains(%s)", label));
        return elements.first();
    }


    private String getElementText(Element mainContent, String label) {
        String cssQuery = String.format(".keyinfobox label:contains(%s)", label);
        return mainContent.select(cssQuery).next().text();
    }

}
